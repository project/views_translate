
Views Translate for Drupal 5.x
==============================

License: http://www.gnu.org/licenses/gpl-2.0.html
Support: Commercial support and customization is available from www.netgenius.co.uk
Email: drupal_dev at netgenius.co.uk



Description:

Performs translation of the page title and labels of fields and exposed filters in Views.


Applications:

Intended for multi-language sites using Views.  Allows a single defined View to work properly with multiple languages.


Configuration:

No configuration required, just install and enable the module in the usual way.


Use:

Your View does not need to be modified in any way.  You will need to define translated strings in the Drupal translation system at (your site)/admin/settings/locale/string/search in the same way as for other applications.


Notes:

In order to translate Views "header", "footer" and "empty" text sections, you can use the Language Sections module, also available via www.netgenius.co.uk

Technical notes:

Operates via documented hooks - hook_views_pre_query and hook_views_pre_view so should not cause compatibility problems.  The actual translation is performed in the standard way, via a call to the Drupal t() function.

