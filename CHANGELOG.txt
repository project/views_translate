
Views Translate module for Drupal 5.x
Allows translation (via t() and locale strings) of the page title and labels of fields and exposed filters in Views.

License: http://www.gnu.org/licenses/gpl-2.0.html
Support: Commercial support and customization is available from www.netgenius.co.uk
Email: drupal_dev at netgenius.co.uk



Thu 18 Sep 2008 10:38:59 AM CEST 

Initial release.

